package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    // TODO Complete me!

    public ModelDuck() {
        setFlyBehavior(new FlyRocketPowered());
        setQuackBehavior(new Squeak());
    }
    public void display(){
        System.out.println("i'm a model duck");
    }

    public void swim(){
        System.out.println("im swimming slow");
    }
}
