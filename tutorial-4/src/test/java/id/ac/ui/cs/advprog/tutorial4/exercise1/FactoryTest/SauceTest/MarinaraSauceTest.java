package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.SauceTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {
    private MarinaraSauce marinaraSauce;

    @Before
    public void setUp() throws Exception{
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Marinara Sauce", marinaraSauce.toString());
    }

}
