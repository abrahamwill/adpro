package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.SauceTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChiliSauce;
import org.junit.Before;
import org.junit.Test;

public class ChiliSauceTest {
    private ChiliSauce chiliSauce;

    @Before
    public void setUp() throws Exception{
        chiliSauce = new ChiliSauce();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Chili Sauce", chiliSauce.toString());
    }

}
