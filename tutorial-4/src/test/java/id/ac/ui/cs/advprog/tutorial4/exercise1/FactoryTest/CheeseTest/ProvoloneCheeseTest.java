package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.CheeseTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ProvoloneCheese;
import org.junit.Before;
import org.junit.Test;

public class ProvoloneCheeseTest {
    private ProvoloneCheese provoloneCheese;

    @Before
    public void setUp() throws Exception{
        provoloneCheese = new ProvoloneCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Provolone Cheese", provoloneCheese.toString());
    }

}
