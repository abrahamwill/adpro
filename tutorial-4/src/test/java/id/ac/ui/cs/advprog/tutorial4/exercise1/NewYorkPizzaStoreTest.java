package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.*;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private PizzaStore nyPizzaStore;
    private Pizza nyCheesePizza;
    private Pizza nyVeggiePizza;
    private Pizza nyClamsPizza;


    @Before
    public void setUp() throws Exception{
        nyPizzaStore = new NewYorkPizzaStore();
        nyCheesePizza = nyPizzaStore.orderPizza("cheese");
        nyVeggiePizza = nyPizzaStore.orderPizza("veggie");
        nyClamsPizza = nyPizzaStore.orderPizza("clam");

    }

    @Test
    public void testCheesePizza(){
        assertEquals("---- New York Style Cheese Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n", nyCheesePizza.toString());
    }

    @Test
    public void testVeggiePizza(){
        assertEquals("---- New York Style Veggie Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Garlic, Onion, Mushrooms, Red Pepper\n", nyVeggiePizza.toString());
    }

    @Test
    public void testClamsPizza(){
        assertEquals("---- New York Style Clam Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Fresh Clams from Long Island Sound\n", nyClamsPizza.toString());
    }



}
