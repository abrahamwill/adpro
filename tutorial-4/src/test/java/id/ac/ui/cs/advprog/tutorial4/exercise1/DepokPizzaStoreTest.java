package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.*;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private PizzaStore depokPizzaStore;
    private Pizza depokCheesePizza;
    private Pizza depokVeggiePizza;
    private Pizza depokClamsPizza;


    @Before
    public void setUp() throws Exception{
        depokPizzaStore = new DepokPizzaStore();
        depokCheesePizza = depokPizzaStore.orderPizza("cheese");
        depokVeggiePizza = depokPizzaStore.orderPizza("veggie");
        depokClamsPizza = depokPizzaStore.orderPizza("clam");

    }

    @Test
    public void testCheesePizza(){
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "Black dough\n" +
                "Chili Sauce\n" +
                "Provolone Cheese\n", depokCheesePizza.toString());
    }

    @Test
    public void testVeggiePizza(){
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "Black dough\n" +
                "Chili Sauce\n" +
                "Provolone Cheese\n" +
                "Broccoli, Onion, Mushrooms, Spinach\n", depokVeggiePizza.toString());

    }

    @Test
    public void testClamsPizza(){
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "Black dough\n" +
                "Chili Sauce\n" +
                "Provolone Cheese\n" +
                "White Clams from Ancol Bay\n", depokClamsPizza.toString());
    }



}
