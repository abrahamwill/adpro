package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.CheeseTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {
    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() throws Exception{
        reggianoCheese = new ReggianoCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Reggiano Cheese", reggianoCheese.toString());
    }

}
