package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.VeggiesTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

public class BroccoliTest {
    private Broccoli broccoli;

    @Before
    public void setUp() throws Exception{
        broccoli = new Broccoli();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Broccoli", broccoli.toString());
    }

}
