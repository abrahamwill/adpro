package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.VeggiesTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

public class EggplantTest {
    private Eggplant eggplant;

    @Before
    public void setUp() throws Exception{
        eggplant = new Eggplant();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Eggplant", eggplant.toString());
    }

}
