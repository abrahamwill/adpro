package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class PizzaTest {
    private PizzaStore depokPizzaStore;
    private PizzaStore depokStore;
    private PizzaStore nyStore;
    private PizzaTestDrive pizzaTestDrive;
    private Pizza depokCheesePizza;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;


    @Before
    public void setUpStreams() {
            System.setOut(new PrintStream(outContent));
            System.setErr(new PrintStream(errContent));
            }

    @After
    public void restoreStreams() {
            System.setOut(originalOut);
            System.setErr(originalErr);
    }

    @Before
    public void setUp() throws Exception{
        depokPizzaStore = new DepokPizzaStore();
        depokCheesePizza = depokPizzaStore.orderPizza("cheese");
        nyStore = new NewYorkPizzaStore();
        depokStore = new DepokPizzaStore();


    }
    @Test
    public void testPizzaTestDriveHasMainMethod() {
        PizzaTestDrive.main(new String[0]);
    }


    @Test
    public void testBake() {
        depokCheesePizza.bake();
        assertEquals("Bake for 25 minutes at 350\n", outContent.toString());
    }

    @Test
    public void testCut() {
        depokCheesePizza.cut();
        assertEquals("Cutting the pizza into diagonal slices\n", outContent.toString());
    }
    @Test
    public void testBox() {
        depokCheesePizza.box();
        assertEquals("Place pizza in official PizzaStore box\n", outContent.toString());
    }

    @Test
    public void testSetterGetterName(){
        depokCheesePizza.setName("bram");
        assertEquals("bram", depokCheesePizza.getName());

    }


}
