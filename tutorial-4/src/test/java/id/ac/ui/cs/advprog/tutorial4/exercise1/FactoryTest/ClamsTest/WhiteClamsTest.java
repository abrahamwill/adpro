package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.ClamsTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.WhiteClams;
import org.junit.Before;
import org.junit.Test;

public class WhiteClamsTest {
    private WhiteClams whiteClams;

    @Before
    public void setUp() throws Exception{
        whiteClams = new WhiteClams();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("White Clams from Ancol Bay", whiteClams.toString());
    }

}
