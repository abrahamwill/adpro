package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.CheeseTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarellaCheese;

    @Before
    public void setUp() throws Exception{
        mozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Shredded Mozzarella", mozzarellaCheese.toString());
    }

}
