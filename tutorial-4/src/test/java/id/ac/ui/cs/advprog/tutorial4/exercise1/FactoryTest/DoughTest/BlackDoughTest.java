package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.DoughTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import org.junit.Before;
import org.junit.Test;

public class BlackDoughTest {
    private BlackDough blackDough;

    @Before
    public void setUp() throws Exception{
        blackDough = new BlackDough();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Black dough", blackDough.toString());
    }

}
