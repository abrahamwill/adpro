package id.ac.ui.cs.advprog.tutorial4.exercise1.FactoryTest.VeggiesTest;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

public class OnionTest {
    private Onion onion;

    @Before
    public void setUp() throws Exception{
        onion = new Onion();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Onion", onion.toString());
    }

}
