package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    // answer : declaration & instantiation

    private static Singleton uniqueInstance; //hanya ada 1 instance untuk class ini

    private Singleton() {
    }

    // Open Closed Principle
    // TODO Implement me!
    public static Singleton getInstance() {
        if (uniqueInstance == null) { //kalo belom pernah dibuat, bikin baru, kalo udah return yang lama
            uniqueInstance = new Singleton(); //Lazy Instantiation
        }
        return uniqueInstance;
    }
}

